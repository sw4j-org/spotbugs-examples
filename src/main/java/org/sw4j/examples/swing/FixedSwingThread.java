/*
 * Copyright (C) 2018 Uwe Plonus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.sw4j.examples.swing;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * Example for "SW: Certain swing methods needs to be invoked in Swing thread"
 * {@code (SW_SWING_METHODS_INVOKED_IN_SWING_THREAD)}.
 */
public class FixedSwingThread {

    public static void main(String... args) {
        JFrame jFrame = new JFrame();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                jFrame.setVisible(true);
            }
        });
    }

}
