# Bad Practice

* [CNT: Rough value of known constant found](#CNT:_Rough_value_of_known_constant_found)
* [BC: Equals method should not assume anything about the type of its argument](#BC:_Equals_method_should_not_assume_anything_about_the_type_of_its_argument)
* [SW: Certain swing methods needs to be invoked in Swing thread](#SW:_Certain_swing_methods_needs_to_be_invoked_in_Swing_thread)
* [BIT: Check for sign of bitwise operation](#BIT:_Check_for_sign_of_bitwise_operation)
* [CN: Class implements Cloneable but does not define or use clone method](#CN:_Class_implements_Cloneable_but_does_not_define_or_use_clone_method)
* [CN: clone method does not call super.clone()](#CN:_clone_method_does_not_call_super.clone.28.29)
* [CN: Class defines clone() but doesn't implement Cloneable](#CN:_Class_defines_clone.28.29_but_doesn.E2.80.99t_implement_Cloneable)
* [Co: Abstract class defines covariant compareTo() method](#Co:_Abstract_class_defines_covariant_compareTo.28.29_method)

## CNT: Rough value of known constant found

### Example

In this example the circumference of a circle is calculated. The value ̣π is is inserted as an approximate value. It is
better to use a constant (e.g. [`Math.PI`](https://docs.oracle.com/javase/8/docs/api/java/lang/Math.html#PI)).

```
        circumference = diameter * 3.141;
```

### Restrictions

This bug is only found when a certain threshold is exceeded (see the not detected example).

### Suppression

This warning can be suppressed with `@SuppressFBWarnings(value="CNT_ROUGH_CONSTANT_VALUE")`.

### Project examples

* Detected [`KnownConstant` Line 27](./xref/org/sw4j/examples/numbers/KnownConstant.html#L27)
* Not Detected [`KnownConstant` Line 31](./xref/org/sw4j/examples/numbers/KnownConstant.html#L31)
* Suppressed [`KnownConstant` Line 34](./xref/org/sw4j/examples/numbers/KnownConstant.html#L34)

### References

[SpotBugs bug description](https://spotbugs.readthedocs.io/en/latest/bugDescriptions.html#cnt-rough-value-of-known-constant-found-cnt-rough-constant-value)


## NP: Method with Boolean return type returns explicit null

### Example

In this example a method with the return type [`Boolean`](https://docs.oracle.com/javase/8/docs/api/java/lang/Boolean.html)
returns `null` directly. This will be found by spotbugs and marked as bug.

```
    public Boolean booleanMethod() {
        return null;
    }
```

### Restrictions

This bug will not be found when a variable is defined which can return `null`.

```
    public Boolean variableBooleanMethod() {
        Boolean result = null;
        return result;
    }
```

### Suppression

This warning can be suppressed with `@SuppressFBWarnings(value="NP_BOOLEAN_RETURN_NULL")`.

### Project examples

* Detected [`BooleanReturnsNull` Line 27](./xref/org/sw4j/examples/nullpointer/BooleanReturnsNull.html#L27)
* Not Detected [`BooleanReturnsNull` Line 32](./xref/org/sw4j/examples/nullpointer/BooleanReturnsNull.html#L32)
* Suppressed [`BooleanReturnsNull` Line 35](./xref/org/sw4j/examples/nullpointer/BooleanReturnsNull.html#L35)

### References

[SpotBugs bug description](https://spotbugs.readthedocs.io/en/latest/bugDescriptions.html#np-method-with-boolean-return-type-returns-explicit-null-np-boolean-return-null)


## SW: Certain swing methods needs to be invoked in Swing thread

### Example

In this example the method `setVisible()` is called from the main thread which can cause deadlocks or calling methods on
partially created Swing components.

```
    public static void main(String... args) {
        JFrame jFrame = new JFrame();
        jFrame.setVisible(true);
    }
```

### Restrictions

This bug is only found when the problematic code is called in the main thread (directly from the `main()` method).

### Fixing

To fix the bug ensure that the problematic methods are called from the event dispatching thread.

```
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                jFrame.setVisible(true);
            }
        });
```

### Suppression

This warning can be suppressed with `@SuppressFBWarnings(value="SW_SWING_METHODS_INVOKED_IN_SWING_THREAD")`.

### Project examples

* Detected [`BugSwingThread` Line 30](./xref/org/sw4j/examples/swing/BugSwingThread.html#L30)
* Not Detected [`BugSwingThread` Line 35](./xref/org/sw4j/examples/swing/BugSwingThread.html#L35)
* Fixed [`FixedSwingThread` Line 31](./xref/org/sw4j/examples/swing/FixedSwingThread.html#L31)
* Suppressed [`SuppressSwingThread` Line 28](./xref/org/sw4j/examples/swing/SuppressSwingThread.html#L28)

### References

[SpotBugs bug description](https://spotbugs.readthedocs.io/en/latest/bugDescriptions.html#sw-certain-swing-methods-needs-to-be-invoked-in-swing-thread-sw-swing-methods-invoked-in-swing-thread)


## BC: Equals method should not assume anything about the type of its argument

### Example

In this example a ClassCastException would be thrown because the object `o` is cast without check.
The [`equals(Object)`](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html#equals-java.lang.Object-)
method should never throw any exception as this violates the contract.

```
    public boolean equals(Object obj) {
        EqualsAssume toCompare = (EqualsAssume)obj;
        ...
    }
```

### Suppression

This warning can be suppressed with `@SuppressFBWarnings(value="BC_EQUALS_METHOD_SHOULD_WORK_FOR_ALL_OBJECTS")`.

### Project examples

* Detected [`EqualsAssume` Line 27](./xref/org/sw4j/examples/equals/EqualsAssume.html#L27)
* Suppressed [`SuppressEqualsAssume` Line 27](./xref/org/sw4j/examples/equals/SuppressEqualsAssume.html#L27)

### References

[SpotBugs bug description](https://spotbugs.readthedocs.io/en/latest/bugDescriptions.html#bc-equals-method-should-not-assume-anything-about-the-type-of-its-argument-bc-equals-method-should-work-for-all-objects)


## BIT: Check for sign of bitwise operation

### Example

In this example the check (`(value & FLAG) > 0`) will fail when `FLAG` is a negative value
(e.g. `0x80000000`). Then the result will be also negative and the check will fail.

```
    private static final int FLAG = 0x40000000;
    public boolean bug(int value) {
        boolean flagSet = ((value & FLAG) > 0);
        ...
    }
```

### Restrictions

This bug will not be found when the `FLAG` is already negative (the highest significant bit is set).

```
    private static final int HIGHEST_FLAG = 0x80000000;
    public boolean highestBitBug(int value) {
        boolean flagSet = ((value & HIGHEST_FLAG) > 0);
        ...
    }
```

### Suppression

This warning can be suppressed with `@SuppressFBWarnings(value="BIT_SIGNED_CHECK")`.

### Project examples

* Detected [`CheckSign` Line 31](./xref/org/sw4j/examples/bitoperation/CheckSign.html#L31)
* Not Detected [`CheckSign` Line 36](./xref/org/sw4j/examples/bitoperation/CheckSign.html#L36)
* Suppressed [`CheckSign` Line 40](./xref/org/sw4j/examples/bitoperation/CheckSign.html#L40)

### References

[SpotBugs bug description](https://spotbugs.readthedocs.io/en/latest/bugDescriptions.html#bit-check-for-sign-of-bitwise-operation-bit-signed-check)


## CN: Class implements Cloneable but does not define or use clone method

### Example

In this example the created class does not override the
[`clone()`](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html#clone--) method.

```
public class NoClone implements Cloneable {
    // no clone()
}
```

### Suppression

This warning can be suppressed with `@SuppressFBWarnings(value="CN_IDIOM")`.

### Project examples

* Detected [`NoClone` Line 22](./xref/org/sw4j/examples/clone/NoClone.html#L22)
* Suppressed [`SuppressNoClone` Line 24](./xref/org/sw4j/examples/clone/SuppressNoClone.html#L24)

### References

[SpotBugs bug description](https://spotbugs.readthedocs.io/en/latest/bugDescriptions.html#cn-class-implements-cloneable-but-does-not-define-or-use-clone-method-cn-idiom)


## CN: clone method does not call super.clone()

### Example

In this example the created class does not call the `clone()` method of the super class. Every class
that implements the `Cloneable` must call the method `super.clone()` to ensure that the `clone()`
returns the correct class.

```
    public Object clone() throws CloneNotSupportedException {
        // no call to super.clone();
    }
```

### Suppression

This warning can be suppressed with `@SuppressFBWarnings(value="CN_IDIOM_NO_SUPER_CALL")`.

### Project examples

* Detected [`NoSuperClone` Line 26](./xref/org/sw4j/examples/clone/NoSuperClone.html#L26)
* Suppressed [`SuppressNoSuperClone` Line 26](./xref/org/sw4j/examples/clone/SuppressNoSuperClone.html#L26)

### References

[SpotBugs bug description](https://spotbugs.readthedocs.io/en/latest/bugDescriptions.html#cn-clone-method-does-not-call-super-clone-cn-idiom-no-super-call)

## CN: Class defines clone() but doesn't implement Cloneable

### Example

In this example the created class overrides the method `clone()` but does not implement the
interface `Cloneable`. This is at least uncommon and not expected.

```
public class NoCloneable {
    public Object clone() throws CloneNotSupportedException {
        ...
    }
}
```

### Suppression

This warning can be suppressed with `@SuppressFBWarnings(value="CN_IMPLEMENTS_CLONE_BUT_NOT_CLONEABLE")`.

### Project examples

* Detected [`NoCloneable` Line 26](./xref/org/sw4j/examples/clone/NoCloneable.html#L26)
* Suppressed [`SuppressNoCloneable` Line 24](./xref/org/sw4j/examples/clone/SuppressNoCloneable.html#L24)

### References

[SpotBugs bug description](https://spotbugs.readthedocs.io/en/latest/bugDescriptions.html#cn-class-defines-clone-but-doesn-t-implement-cloneable-cn-implements-clone-but-not-cloneable)

## Co: Abstract class defines covariant compareTo() method

### Example

In this example the class `Covariant` implements the interface `Comparable` and implements the
method `compareTo` with itself as argument. This is problematic as subclasses cannot override this
method with another argument.

```
public abstract class Covariant implements Comparable<Covariant> {
    @Override
    public abstract int compareTo(Covariant o);
}
```

### Suppression

This warning can be suppressed with `@SuppressFBWarnings(value="CO_ABSTRACT_SELF")`.

### Project examples

* Detected [`Covariant` Line 22](./xref/org/sw4j/examples/compare/Covariant.html#L22)
* Suppressed [`SuppressCovariant` Line 24](./xref/org/sw4j/examples/compare/SuppressCovariant.html#L24)

### References

[SpotBugs bug description](https://spotbugs.readthedocs.io/en/latest/bugDescriptions.html#co-abstract-class-defines-covariant-compareto-method-co-abstract-self)
